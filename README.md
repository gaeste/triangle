# Run the code #

On unix-like system at the root of project:
`./gradlew run -Psides=<a,b,c>`

**Example:**


```
#!shell

./gradlew run -Psides=2,2,3
```


## Task: Tradeshift triangle challenge ##

_Write a program that will determine the type of a triangle.
It should take the lengths of the triangle's three sides as input,
and return whether the triangle is equilateral, isosceles or scalene._

_We are looking for solutions that showcase problem solving skills and
structural considerations that can be applied to larger and potentially
more complex problem domains. Pay special attention to tests, readability of code and error cases._

_The way you reflect upon your decisions is important to us,
why we ask you to include a brief discussion of your design decisions
and implementation choices._


## My Solution ##

__1. Choice of language and tooling__

I'm using Gradle as build system as I've been using it the last years and with the wrapper it is easy to get going.
Also, I'm using Java, since it still is my first goto-language.

For tests, I chose Groovy together with Spock framework. I find it easy to use and with very little effort gets 
high coverage. The readability is also good according to me.

__2. Design choices__

After reading up on Triangle geometry in English I set up some tests and then implemented the logic.
I tried to keep it very simple and not over-engineer it because it is fun. I almost built it with a rule engine, 
Easy Rules, because I'm evaluating it for another task.

But any solution should be easy to evolve from, and I think one could easily: 
 - add web framework to get make it a web service. I would have used Spark framework for show casing.
 - add Docker build. I would have a added a gradle plugin to package the fat jar and run it.
 - make a nicer interactive user experience
 - make builder pattern or fluent api
 - extract interface if similar shapes are considered
 
For the sides, I chose to use integers. Clean, precise and can also represent floating numbers.

I also chose to do validation in the Triangle constructor which I consider could be left out if I trusted the 
data and also knew in which environment I would use the class. This time however I felt the class itself became 
self contained and allowed me to make the business functions easy readable.

I haven't given much thought to operational aspects since I didn't see this piece of code going into a production
system by itself. If incorporating the code into a greater system it would be good to add logging for monitoring.
But that together with metrics are more suitable outside of this code anyhow.

__3. ...final words__

As a developer I should have used Google, Stack Overflow and likewise before I begun coding. Those sources are usually
my best friends when coding. Unfortunately I did not do so until almost finished. I got curious, and of course I did find
similar solutions to the same task and that is something I should have done from the beginning - to be truly efficient :)
One piece of code that I 'stole' was the TriangleType-enum. I realized it would make the usage of the Triangle class
more versatile.