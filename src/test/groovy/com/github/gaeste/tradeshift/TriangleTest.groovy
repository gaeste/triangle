package com.github.gaeste.tradeshift

import com.github.gaeste.tradeshift.triangle.Triangle
import com.github.gaeste.tradeshift.triangle.TriangleType
import spock.lang.Specification
import spock.lang.Unroll

class TriangleTest extends Specification {
    @Unroll
    "Create valid Triangles"() {
        when:
        Triangle t = new Triangle(a, b, c)

        then:
        notThrown(IllegalArgumentException)
        with(t) {
            getA() == a
            getB() == b
            getC() == c
        }

        where:
        a                 | b                 | c
        1                 | 1                 | 1
        3                 | 4                 | 5
        Integer.MAX_VALUE | Integer.MAX_VALUE | Integer.MAX_VALUE
    }

    @Unroll
    "Invalid arguments"() {
        when: "triangle is created with illegal arguments"
        Triangle t = new Triangle(a, b, c)

        then: "exception is thrown"
        IllegalArgumentException iae = thrown()

        where:
        a                 | b                                   | c
        -1                | 1                                   | 1
        1                 | 0                                   | 1
        1                 | 2                                   | 4
        Integer.MAX_VALUE | (Integer.MAX_VALUE / 2).toInteger() | (Integer.MAX_VALUE / 2 + 1).toInteger()
    }

    @Unroll
    "Scalene triangles"() {
        when: "no sides are of same length"
        Triangle t = new Triangle(a, b, c);

        then: "the triangle is Scalene"
        with(t) {
            t.isScalene()
            !t.isIsosceles()
            !t.isEquilateral()
            type == TriangleType.SCALENE
        }
        notThrown(IllegalArgumentException)

        where:
        a | b | c
        2 | 4 | 3
        3 | 2 | 4
        4 | 3 | 2
    }

    @Unroll
    "Isosceles triangles"() {
        when: "two sides are of same length"
        Triangle t = new Triangle(a, b, c)

        then: "the triangle is Isosceles"
        with(t) {
            t.isIsosceles()
            !t.isScalene()
            !t.isEquilateral()
            type == TriangleType.ISOSCELES
        }
        notThrown(IllegalArgumentException)

        where:
        a | b | c
        2 | 3 | 3
        3 | 2 | 3
        3 | 3 | 2
    }

    @Unroll
    "Equilateral triangles"() {
        when: "all sides are of equal length"
        Triangle t = new Triangle(a, b, c)

        then: "triangle is Equilateral"
        with(t) {
            t.isEquilateral()
            !t.isScalene()
            !t.isIsosceles()
            type == TriangleType.EQUILATERAL
        }
        notThrown(IllegalArgumentException)

        where:
        a                 | b                 | c
        2                 | 2                 | 2
        100               | 100               | 100
        Integer.MAX_VALUE | Integer.MAX_VALUE | Integer.MAX_VALUE
    }
}
