package com.github.gaeste.tradeshift;

import com.github.gaeste.tradeshift.triangle.Triangle;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Stream;

@Slf4j
@Value
public class Main {
    private final int a;
    private final int b;
    private final int c;

    public static void main(String... args) {
        if (args.length > 1) {
            usage("Too many arguments");
        }
        if (args.length < 1) {
            usage("Too few arguments");
        }

        String[] sides = args[0].split(",");
        long nrOfIntegerSides = Stream.of(sides).filter(arg -> {
            try {
                Integer.parseInt(arg);
            } catch (Exception e) {
                return false;
            }
            return true;
        }).count();

        if (nrOfIntegerSides != 3) {
            usage("Triangle needs three comma separated integer values as argument");
        }

        int sideA = getInt(sides[0]);
        int sideB = getInt(sides[1]);
        int sideC = getInt(sides[2]);
        try {
            System.out.printf("%nCreating a triangle with sides a=%d, b=%d, c=%d%n", sideA, sideB, sideC);

            Triangle t = new Triangle(sideA, sideB, sideC);
            System.out.printf("%nThe triangle is %s%n", t.getType().name());
        } catch (Exception e) {
            System.err.printf("%nSorry, it is not possible to create such triangle!%n  - %s%n", e.getMessage());
        }
    }

    private static int getInt(String string) {
        return Integer.parseInt(string);
    }

    private static void usage(String msg) {
        System.err.printf("%n%nError: %s", msg);
        System.err.printf("%n%nUsage:%n" +
                "Program takes 1 argument " +
                "and that is the sides of a triangle as comma separated list,%n" +
                "such as 1,2,3 for a Triangle with sides 1, 2 and 3%n");
    }
}