package com.github.gaeste.tradeshift.triangle;

import lombok.Data;

import java.util.stream.Stream;

/**
 * <pre>
 * This class represents a triangle described by its sides: a, b, c.
 * The representation of the lengths is unit less and type used is integer.
 *
 *   |\
 *   | \
 *   |  \
 *   |   \
 * b |    \ c (hypotenuse)
 *   |     \
 *   |      \
 *   |_______\
 *       a
 *
 * Given three sides, it is only a valid triangle if:
 * 1. All sides are > 0
 * 2. The hypotenuse (side 'c') can not be longer than the sum of the other sides 'a' and 'b'
 *
 * A triangle can be categorizrd into one of three types:
 * - Scalene, no sides of same length and no angles of same degree, see isScalene()
 * - Isosceles, two sides are the same length and hence also two angles are the same, see isIsosceles()
 * - Equilateral, all sides are of same length and hence all angles are 60°, see isEquilateral()
 * </pre>
 */

@Data
public class Triangle {
    private final int a;
    private final int b;
    private final int c;

    /**
     * Create a triangle given three sides. Any of the sides can be named 'a', 'b' or 'c'.
     *
     * @param a length of side 'a'
     * @param b length of side 'b'
     * @param c length of side 'c'
     */
    public Triangle(int a, int b, int c) {
        // Validate all args to make business logic trust its data to be sane
        validateArgs(a, b, c);
        validateTriangle(a, b, c);

        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Validate that the sides all are positive integers
     *
     * @param sides the sides of the triangle
     */
    private void validateArgs(Integer... sides) {
        if (Stream.of(sides).anyMatch(side -> side <= 0)) {
            throw new IllegalArgumentException("All sides of a triangle must have a positive length!");
        }
    }

    /**
     * Validate that the hypotenuse isn't longer than the sum of the other sides
     *
     * @param sides the sides of the triangle
     */
    private void validateTriangle(Integer... sides) {
        Integer[] sortedSides = Stream.of(sides).sorted().toArray(Integer[]::new);

        long shortestSidesAggregate = Long.valueOf(sortedSides[0]) + Long.valueOf(sortedSides[1]);
        long longestSide = sortedSides[2];

        if (longestSide >= shortestSidesAggregate) {
            throw new IllegalArgumentException("One side must not be longer than the sum of the other two sides!");
        }
    }

    /**
     * Returns true if the triangle is scalene
     *
     * @return true if the triangle is scalene, false otherwise
     */
    public boolean isScalene() {
        return a != b && b != c && c != a;
    }

    /**
     * Returns true if the triangle is isosceles
     *
     * @return true if the triangle is isosceles, false otherwise
     */
    public boolean isIsosceles() {
        return !isEquilateral() && !isScalene();
    }

    /**
     * Returns true if the triangle is equilateral
     *
     * @return true if the triangle is equilateral, false otherwise
     */
    public boolean isEquilateral() {
        return a == b && b == c;
    }

    /**
     * Returns the type of the Triangle represented as TriangleType enum
     *
     * @return the type of the Triangle
     */
    public TriangleType getType() {
        if (isEquilateral()) return TriangleType.EQUILATERAL;
        if (isIsosceles()) return TriangleType.ISOSCELES;
        return TriangleType.SCALENE;
    }
}
