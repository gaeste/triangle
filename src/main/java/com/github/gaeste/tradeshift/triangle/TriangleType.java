package com.github.gaeste.tradeshift.triangle;

public enum TriangleType {
    EQUILATERAL, ISOSCELES, SCALENE
}
